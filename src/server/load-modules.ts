/**
 * Loads the application modules. Required for controllers and services registration.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */

import "./core";
import "./customer";
