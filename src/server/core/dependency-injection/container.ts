import { Container } from "inversify";

/**
 * ### Dependency injection Container.
 *
 * For eache HTTP request, the package <code>@weefsell/inversify-express</code> will create a child
 * container, allowing the redefinition of the bindings for that request context.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export const container = new Container({skipBaseClassChecks: true});
