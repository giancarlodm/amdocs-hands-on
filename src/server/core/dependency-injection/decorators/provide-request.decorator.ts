import { injectable, interfaces } from "inversify";

import { container } from "../container";

/**
 * @Decorator
 * Registers a class with the given an identifier on the dependency injection container in a request
 * scope (i.e. during a request to the container, if multiple objects depends on this type, only one
 * instance will be created and injected on them).
 * @param identifier Identification token to be used with the container.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export function ProvideRequest(identifier?: interfaces.ServiceIdentifier<any>) {

    return (constructor: interfaces.Newable<any>) => {

        container.bind(identifier)
            .to(constructor)
            .inRequestScope();

        return injectable()(constructor);
    };
}
