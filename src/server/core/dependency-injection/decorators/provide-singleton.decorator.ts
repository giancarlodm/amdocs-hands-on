import { injectable, interfaces } from "inversify";

import { container } from "../container";

/**
 * @Decorator
 * Registers a class with the given an identifier on the dependency injection container in a sigleton
 * scope (i.e. one instance for the entire application lifecycle).
 * @param identifier Identification token to be used with the container.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export function ProvideSingleton(identifier?: interfaces.ServiceIdentifier<any>) {

    return (constructor: interfaces.Newable<any>) => {

        if (identifier == null) {
            identifier = constructor;
        }

        container.bind(identifier)
            .to(constructor)
            .inSingletonScope();

        return injectable()(constructor);
    };
}
