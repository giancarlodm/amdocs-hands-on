import { ProvideSingleton } from "../../../dependency-injection";
import { IEntityManager } from "../entity-manager.service.interface";

/**
 * @ProvideSingleton
 * See {@link IEntityManager}.
 * A mock Entity Manager based on the TypeORM Entity Manager service.
 * We provide it as a singleton, assuming there is only one database. If multiple databases are used,
 * we should use another strategy, such as a context driven DI based on some condition (e.g. API key,
 * user, etc).
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@ProvideSingleton(IEntityManager)
class EntityManager implements IEntityManager {

    //#region Private Attributes
    /**
     * Represents a database. A place to store entities. A map of "tables" (entity class) with "rows"
     * (objects identified by an id").
     */
    private readonly database: Map<Function, Map<any, {id: any}>>;
    /**
     * Sequence generator. A map of current sequence number of a given entity.
     */
    private readonly sequences: Map<Function, number>;
    //#endregion

    //#region Constructor
    constructor() {

        this.database = new Map<Function, Map<any, {id: any}>>();
        this.sequences = new Map<Function, number>();
    }
    //#endregion

    //#region IEntityManager Methods
    /**
     * @inheritDoc
     */
    public async count<T extends {id: any}>(entityClass: Function, conditions?: Partial<T>,
                                              options?: {limit: number, skip: number}): Promise<number> {

        if (options?.limit === 0) {
            return 0;
        }

        const table: Map<any, T> = await this.getTable(entityClass);

        let count: number = 0;
        if (conditions != null) {

            if (table.has(conditions.id)) {
                return 1;
            }
            else {

                const conditionsKeys: Array<PropertyKey> = Reflect.ownKeys(conditions);

                let skipped: number = 0;
                for (let row of table.values()) {

                    let keep: boolean = true;
                    for (let key of conditionsKeys) {

                        const rowValue: any = Reflect.get(row, key);
                        const conditionValue: any = Reflect.get(conditions, key);

                        if (rowValue !== conditionValue) {
                            keep = false;
                            break;
                        }
                    }

                    if (keep) {

                        if (skipped < options?.skip) {
                            skipped++;
                            continue;
                        }

                        count++;
                    }

                    if (count >= options?.limit) {
                        return count;
                    }
                }

                return count;
            }
        }

        return table.size;
    }

    /**
     * @inheritDoc
     */
    public async find<T extends {id: any}>(entityClass: Function, conditions?: Partial<T>,
                                        options?: { limit: number; skip: number }): Promise<Array<T>> {

        const table: Map<any, T> = await this.getTable<T>(entityClass);
        let rows = Array.from(table.values());

        if (conditions != null) {

            const conditionsKeys: Array<PropertyKey> = Reflect.ownKeys(conditions);

            rows = rows.filter(row => {

                for (let key of conditionsKeys) {

                    const rowValue: any = Reflect.get(row, key);
                    const conditionValue: any = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        return false;
                    }
                }

                return true;
            });
        }

        if (options != null) {

            rows.splice(0, options.skip);
            rows.splice(options.limit);
        }

        return rows;
    }

    /**
     * @inheritDoc
     */
    public async save<T extends {id: any}>(entity: T): Promise<T> {

        const constructor: Function = entity.constructor;
        const table: Map<any, T> = await this.getTable(constructor);

        // its insertion (we allow null IDs)
        if (entity.id === undefined || !table.has(entity.id)) {
            entity.id = this.getSequence(constructor);
            table.set(entity.id, entity);
        }
        else {
            table.set(entity.id, entity);
        }

        return entity;
    }

    /**
     * @inheritDoc
     */
    public async delete<T extends {id: any}>(entityClass: Function, conditions: Partial<T>): Promise<{removed: number}> {

        const table: Map<any, T> = await this.getTable(entityClass);

        if (conditions.id !== undefined && !table.has(conditions.id)) {
            return {removed: 0};
        }

        const conditionsKeys: Array<PropertyKey> = Reflect.ownKeys(conditions);
        if (conditionsKeys.includes("id") && conditionsKeys.length === 1) {

            table.delete(conditions.id);
            return {removed: 1};
        }
        else {

            let removed: number = 0;
            for (let row of table.entries()) {

                let removeFlag: boolean = true;
                for (let key of conditionsKeys) {

                    const rowValue: any = Reflect.get(row[1], key);
                    const conditionValue: any = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        removeFlag = false;
                        break;
                    }
                }

                if (removeFlag) {
                    table.delete(row[0]);
                    removed++;
                }
            }

            return {removed: removed};
        }

    }

    /**
     * @inheritDoc
     */
    public async update<T extends {id: any}>(entityClass: Function, conditions: Partial<T>,
                                               update: Partial<T>): Promise<{ modified: number }> {

        const table: Map<any, T> = await this.getTable(entityClass);

        if (conditions.id !== undefined && !table.has(conditions.id)) {
            return {modified: 0};
        }

        const conditionsKeys: Array<PropertyKey> = Reflect.ownKeys(conditions);
        if (conditionsKeys.includes("id") && conditionsKeys.length === 1) {

            const row: T = table.get(conditions.id);
            const updateKeys: Array<PropertyKey> = Reflect.ownKeys(update);
            for (let key of updateKeys) {
                Reflect.set(row, key, Reflect.get(update, key));
            }

            return {modified: 1};
        }
        else {

            let modified: number = 0;
            for (let row of table.values()) {

                let updateFlag: boolean = true;
                for (let key of conditionsKeys) {

                    const rowValue: any = Reflect.get(row, key);
                    const conditionValue: any = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        updateFlag = false;
                        break;
                    }
                }

                if (updateFlag) {

                    const updateKeys: Array<PropertyKey> = Reflect.ownKeys(update);
                    for (let key of updateKeys) {
                        Reflect.set(row, key, Reflect.get(update, key));
                    }

                    modified++;
                }
            }

            return {modified: modified};
        }
    }
    //#endregion

    //#region Private Methods
    /**
     * Obtain the row map of a given entity. Creates the map if not yet defined.
     * @param entityClass The entity Class
     * @return  Row Map of the entity class.
     */
    private getTable<T extends {id: any}>(entityClass: Function): Promise<Map<any, T>> {

        if (!this.database.has(entityClass)) {
            this.database.set(entityClass, new Map<any, {id: any}>());
            this.sequences.set(entityClass, 1);
        }

        return Promise.resolve(this.database.get(entityClass) as Map<any, T>);
    }

    /**
     * Generates a sequence for a given entity class
     * @param entityClass The entity class to generate the next value.
     * @return The generated sequence value.
     */
    private getSequence(entityClass: Function): number {

        const current: number = this.sequences.get(entityClass);
        this.sequences.set(entityClass, current + 1);

        return current;
    }
    //#endregion
}
