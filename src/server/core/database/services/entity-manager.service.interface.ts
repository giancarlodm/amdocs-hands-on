/**
 * Represents a generic interface of an ORM framework Entity Manager do fremework ORM, serving as
 * a mock Entity Manager. Based on TypeORM Entity Manager.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface IEntityManager {

    /**
     * Counts the number of rows of a given entity.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The entity class to obtain row count.
     * @param conditions Conditions to apply.
     * @param options Pagination options.
     * @return Row count
     */
    count<T extends {id: any}>(entityClass: Function, conditions?: Partial<T>,
                               options?: {limit: number, skip: number}): Promise<number>;
    /**
     * Retrieves a list of entities.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The entity class to obtain row count.
     * @param conditions Conditions to apply.
     * @param options Pagination options.
     * @return List of entities
     */
    find<T extends {id: any}>(entityClass: Function, conditions?: Partial<T>,
                              options?: {limit: number, skip: number}): Promise<Array<T>>;
    /**
     * Persists an entity. Performs insertion or update. Setting an sequence generated ID, if necessary,
     * mutating the object.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entity The entity to persist
     * @return The saved entity. Same object reference as the
     */
    save<T extends {id: any}>(entity: T): Promise<T>;
    /**
     * Deletes an entity.
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The class of the entity to delete
     * @param conditions Delete conditions.
     * @return Number of deleted rows
     */
    delete<T extends {id: any}>(entityClass: Function, conditions: Partial<T>): Promise<{removed: number}>;
    /**
     * Updates an entity. Differs from {@link save} in a sense that a partial entity can be used,
     * does not perform insertions and can update multiples rows.
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The class of the entity to update
     * @param conditions Update conditions.
     * @param update The update to perform.
     * @return Number of modified rows
     */
    update<T extends {id: any}>(entityClass: Function, conditions: Partial<T>, update: Partial<T>): Promise<{ modified: number }>;
}

export const IEntityManager: symbol = Symbol("IEntityManager");
