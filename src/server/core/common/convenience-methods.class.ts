/**
 * Static class with convenience methods.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class ConvenienceMethods {

    /**
     * Retrieve an enumerator values as an Array.
     * ###
     * Generic Types:
     * - ``C`` - The enumerator type.
     * - ``M`` - The type of the enum value.
     * @param enumerator The enumerator to retrieve values.
     * @returns The enumerator values.
     */
    public static getEnumeratorValues<C extends Object, M>(enumerator: C): M[] {

        const values: M[] = [];
        const keys: PropertyKey[] = Reflect.ownKeys(enumerator);
        for (let key of keys) {

            if (!Number.isNaN(Number(key))) {
                continue;
            }

            values.push(Reflect.get(enumerator, key));
        }

        return values;
    }
}
