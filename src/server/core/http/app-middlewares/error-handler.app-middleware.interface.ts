import * as express from "express";

/**
 * Application Middleware for error treatment. Treats errors generated during requests, returning an
 * appropriate message to the client.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface IErrorHandlerAppMiddleware {

    /**
     * The express error request handler
     * @param error Error occurred
     * @param req Request object
     * @param res Response object
     * @param next Callback to call next handler on the stack
     */
    handler(error: Error, req: express.Request, res: express.Response, next: express.NextFunction): void;
}

export const IErrorHandlerAppMiddleware: symbol = Symbol("IErrorHandlerAppMiddleware");
