import * as express from "express";

import { ProvideSingleton } from "../../../dependency-injection";
import {
    HttpException, HttpInternalServerErrorException
} from "../../exceptions";
import { HttpServerErrorStatusEnum } from "../../status";
import { IErrorHandlerAppMiddleware } from "../error-handler.app-middleware.interface";

/**
 * @ProvideSingleton
 * See {@link IErrorHandlerAppMiddleware}
 *
 * @sinceVerion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@ProvideSingleton(IErrorHandlerAppMiddleware)
class ErrorHandlerAppMiddleware implements IErrorHandlerAppMiddleware {

    /**
     * @inheritDoc
     */
    public handler(error: Error, req: express.Request, res: express.Response, next: express.NextFunction): void {

        // If the exceptions is an HttpException, return the exception with its code
        if (error instanceof HttpException) {

            res.status(error.status).json(error);
        }
        // Else, 500 Internal Server Error
        else {

            const internalServerErrorException: HttpInternalServerErrorException<any> = new HttpInternalServerErrorException<any>("Oops! An server error occurred", {
                name: error.name,
                message: error.message
            });

            res.status(HttpServerErrorStatusEnum.INTERNAL_SERVER_ERROR).json(internalServerErrorException);

            console.error(error);
        }
    }
}
