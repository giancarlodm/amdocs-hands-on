import { HttpServerErrorStatusEnum } from "../../status";
import { HttpException } from "../http-exception.class";

/**
 * The 500 Internal Server Error exceptiom. If details is an instance of {@link Error}, its stack
 * will be serialized.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class HttpInternalServerErrorException<T> extends HttpException<T> {

    constructor();
    constructor(message: string);
    constructor(message: string, details: T)
    constructor(...args: []|[string]|[string, T]) {
        super(HttpServerErrorStatusEnum.INTERNAL_SERVER_ERROR, args[0], args[1]);
    }

    /**
     * @inheritDoc
     * Adds the stack trace if {@link details} is an instace of {@link Error}
     */
    public toJSON(): Partial<HttpException<T>> {

        if (this.details != null && this.details instanceof Error) {

            return {
                ...super.toJSON(),
                details: {
                    ...this.details as Error,
                    name: this.details.name,
                    message: this.details.message,
                    stack: this.details.stack
                } as any
            };
        }
        else {
            return super.toJSON();
        }
    }
}
