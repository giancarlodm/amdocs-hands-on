import { IllegalArgumentException, Exception} from "@enterprize/exceptions";

import { HttpErrorStatus, HttpStatusConvenienceMethods } from "../status";

/**
 * Base class for HTTP exceptions.
 * ###
 * Generic Types:
 * - ``T`` - (optional) The type of the details
 *
 * @since 02/03/2020
 */
export abstract class HttpException<T = void> extends Exception<T> {

    //#region Public Attributes
    /**
     * HTTP status code. Must be a 4xx or a 5xx.
     */
    public readonly status: HttpErrorStatus;
    /**
     * HTTP legible status name.
     */
    public readonly statusName: string;
    //#endregion

    //#region Constructor
    protected constructor(status: HttpErrorStatus, message?: string, details?: T) {

        if (status < 400 || status > 500) {
            throw new IllegalArgumentException("The HTTP \"status\" for errors must be of class 4xx or 5xx", "status");
        }

        super(message, details);

        this.status = status;
        this.statusName = HttpStatusConvenienceMethods.getStatusName(status);
    }
    //#endregion

    //#region Public Methods
    /**
     * Customizes the JSON serialization. Called automatically by JSON.stringfy.
     */
    public toJSON(): Partial<HttpException<T>> {

        return {
            status: this.status,
            statusName: this.statusName != null ? this.statusName : "",
            message: this.message,
            details: this.details as any
        };
    }
    //#endregion
}
