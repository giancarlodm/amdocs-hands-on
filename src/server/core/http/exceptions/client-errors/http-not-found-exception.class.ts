import { HttpClientErrorStatusEnum } from "../../status";
import { HttpException } from "../http-exception.class";

/**
 * The 404 Not Found HTTP Exception.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class HttpNotFoundException<T = Object> extends HttpException<T> {

    constructor(message?: string, details?: T) {
        super(HttpClientErrorStatusEnum.NOT_FOUND, message, details);
    }
}
