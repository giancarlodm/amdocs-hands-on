import { HttpClientErrorStatusEnum } from "../../status";
import { HttpException } from "../http-exception.class";

/**
 * The 400 Bad Request HTTP exception.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class HttpBadRequestException<T = Object> extends HttpException<T> {

    constructor();
    constructor(message: string);
    constructor(message: string, details: T);
    constructor(...args: []|[string]|[string, T]) {
        super(HttpClientErrorStatusEnum.BAD_REQUEST, args[0], args[1]);
    }
}
