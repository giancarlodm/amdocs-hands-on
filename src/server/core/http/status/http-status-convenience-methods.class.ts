import { HttpInformationalStatusEnum } from "./http-informational-status.enum";
import { HttpSuccessStatusEnum } from "./http-success-status.enum";
import { HttpRedirectionStatusEnum } from "./http-redirection-status.enum";
import { HttpClientErrorStatusEnum } from "./http-client-error-status.enum";
import { HttpServerErrorStatusEnum } from "./http-server-error-status.enum";
import { HttpStatusCodes } from "./http-status-code.type";

/**
 * Static class with convenience methods for handling HTTP status.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class HttpStatusConvenienceMethods {

    //#region Private Static Attributes
    /**
     * Mapa de nomes legíveis dos status HTTP a partir do código numérico do status
     */
    private static readonly httpStatusNamesMap: Map<number, string> = new Map<number, string>([

        // Informacional
        [HttpInformationalStatusEnum.CONTINUE, "Continue"],
        [HttpInformationalStatusEnum.SWITCHING_PROTOCOLS, "Switching Protocols"],
        [HttpInformationalStatusEnum.PROCESSING, "Processing"],
        [HttpInformationalStatusEnum.EARLY_HINTS, "Early Hints"],

        // Sucesso
        [HttpSuccessStatusEnum.OK, "OK"],
        [HttpSuccessStatusEnum.CREATED, "Created"],
        [HttpSuccessStatusEnum.ACCEPTED, "Accepted"],
        [HttpSuccessStatusEnum.NON_AUTHORITATIVE_INFORMATION, "Non-Authoritative Information"],
        [HttpSuccessStatusEnum.NO_CONTENT, "No Content"],
        [HttpSuccessStatusEnum.RESET_CONTENT, "Reset Content"],
        [HttpSuccessStatusEnum.PARTIAL_CONTENT, "Partial Content"],
        [HttpSuccessStatusEnum.MULTI_STATUS, "Multi-Status"],
        [HttpSuccessStatusEnum.ALREADY_REPORTED, "Already Reported"],
        [HttpSuccessStatusEnum.IM_USED, "IM Used"],

        // Redirecionamento
        [HttpRedirectionStatusEnum.MULTIPLE_CHOICES, "Multiple Choices"],
        [HttpRedirectionStatusEnum.MOVED_PERMANENTLY, "Moved Permanently"],
        [HttpRedirectionStatusEnum.FOUND, "Found"],
        [HttpRedirectionStatusEnum.SEE_OTHER, "See Other"],
        [HttpRedirectionStatusEnum.NOT_MODIFIED, "Not Modified"],
        [HttpRedirectionStatusEnum.USE_PROXY, "Use Proxy"],
        [HttpRedirectionStatusEnum.SWITCH_PROXY, "Switch Proxy"],
        [HttpRedirectionStatusEnum.TEMPORARY_REDIRECT, "Temporary Redirect"],
        [HttpRedirectionStatusEnum.PERMANENT_REDIRECT, "Permanent Redirect"],

        // Erros de cliente
        [HttpClientErrorStatusEnum.BAD_REQUEST, "Bad Request"],
        [HttpClientErrorStatusEnum.UNAUTHORIZED, "Unauthorized"],
        [HttpClientErrorStatusEnum.PAYMENT_REQUIRED, "Payment Required"],
        [HttpClientErrorStatusEnum.FORBIDDEN, "Forbidden"],
        [HttpClientErrorStatusEnum.NOT_FOUND, "Not Found"],
        [HttpClientErrorStatusEnum.METHOD_NOT_ALLOWED, "Method Not Allowed"],
        [HttpClientErrorStatusEnum.NOT_ACCEPTABLE, "Not Acceptable"],
        [HttpClientErrorStatusEnum.PROXY_AUTHENTICATION_REQUIRED, "Proxy Authentication Required"],
        [HttpClientErrorStatusEnum.REQUEST_TIMEOUT, "Request Timeout"],
        [HttpClientErrorStatusEnum.CONFLICT, "Conflict"],
        [HttpClientErrorStatusEnum.GONE, "Gone"],
        [HttpClientErrorStatusEnum.LENGTH_REQUIRED, "Length Required"],
        [HttpClientErrorStatusEnum.PRECONDITION_FAILED, "Precondition Failed"],
        [HttpClientErrorStatusEnum.PAYLOAD_TOO_LARGE, "Payload Too Large"],
        [HttpClientErrorStatusEnum.URI_TOO_LONG, "URI Too Long"],
        [HttpClientErrorStatusEnum.UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"],
        [HttpClientErrorStatusEnum.RANGE_NOT_SATISFIABLE, "Range Not Satisfiable"],
        [HttpClientErrorStatusEnum.EXPECTATION_FAILED, "Expectation Failed"],
        [HttpClientErrorStatusEnum.IM_A_TEAPOT, "I'm a teapot"],
        [HttpClientErrorStatusEnum.MISDIRECTED_REQUEST, "Misdirected Request"],
        [HttpClientErrorStatusEnum.UNPROCESSABLE_ENTITY, "Unprocessable Entity"],
        [HttpClientErrorStatusEnum.LOCKED, "Locked"],
        [HttpClientErrorStatusEnum.FAILED_DEPENDENCY, "Failed Dependency"],
        [HttpClientErrorStatusEnum.UPGRADE_REQUIRED, "Upgrade Required"],
        [HttpClientErrorStatusEnum.PRECONDITION_REQUIRED, "Precondition Required"],
        [HttpClientErrorStatusEnum.TOO_MANY_REQUESTS, "Too Many Requests"],
        [HttpClientErrorStatusEnum.REQUEST_HEADER_FIELDS_TOO_LARGE, "Request Header Fields Too Large"],
        [HttpClientErrorStatusEnum.UNAVAILABLE_FOR_LEGAL_REASONS, "Unavailable For Legal Reasons"],

        // Erros de Servidor
        [HttpServerErrorStatusEnum.INTERNAL_SERVER_ERROR, "Internal Server Error"],
        [HttpServerErrorStatusEnum.NOT_IMPLEMENTED, "Not Implemented"],
        [HttpServerErrorStatusEnum.BAD_GATEWAY, "Bad Gateway"],
        [HttpServerErrorStatusEnum.SERVICE_UNAVAILABLE, "Service Unavailable"],
        [HttpServerErrorStatusEnum.GATEWAY_TIMEOUT, "Gateway Timeout"],
        [HttpServerErrorStatusEnum.HTTP_VERSION_NOT_SUPPORTED, "HTTP Version Not Supported"],
        [HttpServerErrorStatusEnum.VARIANT_ALSO_NEGOTIATES, "Variant Also Negotiates"],
        [HttpServerErrorStatusEnum.INSUFFICIENT_STORAGE, "Insufficient Storage"],
        [HttpServerErrorStatusEnum.LOOP_DETECTED, "Loop Detected"],
        [HttpServerErrorStatusEnum.NOT_EXTENDED, "Not Extended"],
        [HttpServerErrorStatusEnum.NETWORK_AUTHENTICATION_REQUIRED, "Network Authentication Required"]
    ]);
    //#endregion

    //#region Public Static Methods
    /**
     * Retrieve the legible name of the HTTP status given its code.
     * @param status The HTTP status code to retrive the legible name.
     * @return Legible name of the HTTP status or <code>undefined</code> if the code is not registered.
     */
    public static getStatusName(status: HttpStatusCodes): string {
        return HttpStatusConvenienceMethods.httpStatusNamesMap.get(status);
    }

    /**
     * Define or redefine a legible name for a given HTTP status.
     * @param status The status to define a legible name.
     * @param name The legible name
     */
    public static setStatusName(status: HttpStatusCodes, name: string): void {
        HttpStatusConvenienceMethods.httpStatusNamesMap.set(status, name);
    }
    //#endregion
}
