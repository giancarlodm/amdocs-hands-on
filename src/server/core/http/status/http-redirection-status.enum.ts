/**
 * HTTP redirection (3xx) codes enum.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export enum HttpRedirectionStatusEnum {

    /**
     * Cacheable: True
     */
    MULTIPLE_CHOICES = 300,
    /**
     * Cacheable: True
     */
    MOVED_PERMANENTLY = 301,
    FOUND = 302,
    SEE_OTHER = 303,
    NOT_MODIFIED = 304,
    USE_PROXY = 305,
    SWITCH_PROXY = 306,
    TEMPORARY_REDIRECT = 307,
    PERMANENT_REDIRECT = 308
}
