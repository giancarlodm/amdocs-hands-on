/**
 * HTTP success (2xx) status codes.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export enum HttpSuccessStatusEnum {

    /**
     * Cacheable: True
     */
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    /**
     * Cacheable: True
     */
    NON_AUTHORITATIVE_INFORMATION = 203,
    /**
     * Cacheable: True
     */
    NO_CONTENT= 204,
    RESET_CONTENT = 205,
    /**
     * Cacheable: True
     */
    PARTIAL_CONTENT = 206,
    MULTI_STATUS = 207,
    ALREADY_REPORTED = 208,
    IM_USED = 226
}
