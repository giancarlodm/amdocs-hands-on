import { HttpClientErrorStatusEnum } from "./http-client-error-status.enum";
import { HttpInformationalStatusEnum } from "./http-informational-status.enum";
import { HttpRedirectionStatusEnum } from "./http-redirection-status.enum";
import { HttpServerErrorStatusEnum } from "./http-server-error-status.enum";
import { HttpSuccessStatusEnum } from "./http-success-status.enum";

/**
 * Convenience type with HTTP status codes.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export type HttpStatusCodes = HttpInformationalStatusEnum | HttpSuccessStatusEnum | HttpRedirectionStatusEnum |
    HttpClientErrorStatusEnum | HttpServerErrorStatusEnum | number;
