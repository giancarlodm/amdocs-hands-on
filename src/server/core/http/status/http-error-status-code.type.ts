import { HttpClientErrorStatusEnum } from "./http-client-error-status.enum";
import { HttpServerErrorStatusEnum } from "./http-server-error-status.enum";

/**
 * Convenience type with the accepted HTTP error codes.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export type HttpErrorStatus = HttpClientErrorStatusEnum | HttpServerErrorStatusEnum | number;
