import { BaseMiddleware } from "@weefsell/inversify-express";
import { ValidateFunction } from "ajv";
import * as express from "express";
import { inject as Inject } from "inversify";

import { ProvideRequest } from "../../../dependency-injection";
import { HttpBadRequestException } from "../../../http";
import { IJsonSchemaValidatorService } from "../../services";
import { IRequestValidatorMiddleware } from "../request-validator.middleware.interface";
import { RequestSchema } from "../request-schema.interface";

/**
 * @ProvideRequest
 * {@link IRequestValidatorMiddleware}
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@ProvideRequest(IRequestValidatorMiddleware)
class RequestValidatorMiddleware extends BaseMiddleware implements IRequestValidatorMiddleware {

    //#region Private Attributes
    /**
     * Json schema validation service
     * @injected
     */
    private readonly jsonSchemaValidatorService: IJsonSchemaValidatorService;
    //#endregion

    //#region Constructor
    constructor(@Inject(IJsonSchemaValidatorService) jsonSchemaValidatorService: IJsonSchemaValidatorService) {
        super();

        // Injected
        this.jsonSchemaValidatorService = jsonSchemaValidatorService;
    }
    //#endregion

    //#region Middleware Handler
    /**
     * @inheritDoc
     */
    public async handler(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {

        //request schema
        const requestSchema: RequestSchema = this.middlewareContext;

        if (requestSchema.headers != null) {

            const validateFunction: ValidateFunction = await this.jsonSchemaValidatorService.compile(requestSchema.headers);
            if (!validateFunction(req.headers)) {
                throw new HttpBadRequestException("The request \"headers\" did not pass JSON Schema validation.", validateFunction.errors);
            }
        }

        if (requestSchema.params != null) {

            const validateFunction: ValidateFunction = await this.jsonSchemaValidatorService.compile(requestSchema.params);
            if (!validateFunction(req.params)) {
                throw new HttpBadRequestException("The request \"URL parameters\" did not pass JSON Schema validation.", validateFunction.errors);
            }
        }

        if (requestSchema.query != null) {

            const validateFunction: ValidateFunction = await this.jsonSchemaValidatorService.compile(requestSchema.query);
            if (!validateFunction(req.query)) {
                throw new HttpBadRequestException("The request \"query parameters\" did not pass JSON Schema validation.", validateFunction.errors);
            }
        }

        if (requestSchema.body != null) {

            const validateFunction: ValidateFunction = await this.jsonSchemaValidatorService.compile(requestSchema.body);
            if (!validateFunction(req.body)) {
                throw new HttpBadRequestException("The request \"body\" did not pass JSON Schema validation.", validateFunction.errors);
            }
        }

        next();
    }
    //#endregion
}
