/**
 * JSON schema configuration interface for HTTP requests.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface RequestSchema {

    /**
     * JSON schema for request headers.
     */
    headers?: Object;
    /**
     * JSON schema for request parameters.
     */
    params?: Object;
    /**
     * JSON schema for request query parameters.
     */
    query?: Object;
    /**
     * JSON schema for request body validation.
     */
    body?: Object;
}
