/**
 * Request validation middleware.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface IRequestValidatorMiddleware {

}

export const IRequestValidatorMiddleware: symbol = Symbol("IRequestValidatorMiddleware");

