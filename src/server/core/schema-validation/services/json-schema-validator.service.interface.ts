import { ValidateFunction } from "ajv";

/**
 * JSON schema validation service. Uses AJV to perform validation.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface IJsonSchemaValidatorService {

    /**
     * Compiles a JSON schema, returning an validation function.
     * @param jsonSchema The JSON schema to be compiled
     * @return Function to be used to validate.
     */
    compile(jsonSchema: Object): Promise<ValidateFunction>;
}

export const IJsonSchemaValidatorService: symbol = Symbol("IJsonSchemaValidatorService");
