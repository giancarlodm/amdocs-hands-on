import * as Ajv from "ajv";

import { ProvideSingleton } from "../../../dependency-injection";
import { IJsonSchemaValidatorService } from "../json-schema-validator.service.interface";

/**
 * @ProvideSingleton
 * See {@link IJsonSchemaValidatorService}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@ProvideSingleton(IJsonSchemaValidatorService)
export class JsonSchemaValidatorService implements IJsonSchemaValidatorService {

    //#region Private Attributes
    /**
     * The AJV instance.
     */
    private ajv: Ajv.Ajv;
    //#endregion

    //#region Constructor
    constructor() {

        // Privados
        this.ajv = new Ajv({format: "full", useDefaults: true, coerceTypes: true, loadSchema: this.loadSchema.bind(this)});
    }
    //#endregion

    //#region Public Methods
    /**
     * @inheritDoc
     */
    public compile(jsonSchema: Object): Promise<Ajv.ValidateFunction> {

        return this.ajv.compileAsync(jsonSchema) as Promise<Ajv.ValidateFunction> ;
    }
    //#endregion

    //#region Private Methods
    /**
     * Loads remote schemas refs
     * @param uri The schema URI to load
     */
    private async loadSchema(uri: string): Promise<Object> {

        // TODO

        return null;
    }
    //#endregion
}
