import "./polyfills";
import "./load-modules";

import * as cluster from "cluster";
import * as http from "http";

import { InversifyExpressServer } from "@weefsell/inversify-express";
import compression = require("compression");
import * as express from "express";
import rateLimit = require("express-rate-limit");

import { container } from "./core/dependency-injection";
import { IErrorHandlerAppMiddleware } from "./core/http/app-middlewares";

init()
    .then(() => {

        // If using in a cluster (PM2 managed), we can send the "ready" message to the master signaling
        // that this worker is ready
        if (cluster.isWorker) {
            process.send("ready");
        }
    })
    .catch((error: Error) => {
        console.error(error);
        process.exit(1);
    });

/**
 * Initialization script.
 */
async function init(): Promise<void> {

    console.log("\n=============== Starting Amdocs Hands-On Server ===============");
    console.log(`Environment: ${process.env.NODE_ENV}`);
    console.log(`Timestamp: ${(new Date()).toString()}`);

    await validateEnvironmentVars();
    await configureContainer();
    const inversifyExpressServer: InversifyExpressServer = await configureServer();
    const httpServer: http.Server = await startServer(inversifyExpressServer);
    await configureShutdown(httpServer);

    console.log("========================================================================");
}

/**
 * Validates environment variables.
 */
async function validateEnvironmentVars(): Promise<void> {

    console.log("Validating Environment Variables...");


    // NODE_ENV
    if (process.env.NODE_ENV == null || (process.env.NODE_ENV != "development" && process.env.NODE_ENV != "production")) {
        throw new Error("NODE_ENV environment variable must be set to \"development\" or \"production\"");
    }

    // PORT
    if (process.env.PORT == null || (isNaN(Number(process.env.PORT))) || Number(process.env.PORT) < 1024 || Number(process.env.PORT) > 65535) {
        throw new Error("\"PORT\" environment variable must be set to a number greater or equal than 1024 and less or equal than 65535");
    }

    console.log("\tOK!");
}

/**
 * Configure the dependency injection container.
 */
async function configureContainer(): Promise<void> {

    console.log("Configuring IoC Container...");

    // Efetuar bindo do Enterprize Serializer no container de injeção de dependência
    // Reflect.decorate([injectable()], Serializer);
    // container.bind(ISerializer).to(Serializer).inSingletonScope();

    console.log("\tOK!");
}

/**
 * Configure application server.
 */
async function configureServer(): Promise<InversifyExpressServer> {

    console.log("Configuring Server...");

    /**
     * The ExpressJS server wrapped with Inversify.
     */
    const server: InversifyExpressServer = new InversifyExpressServer(container, null, {rootPath: "/api"});

    // Configuration to be applied to the server
    server.setConfig((app: express.Application) => {

        //--- Application Middlewares

        // If Public REST API, a CORS policy must be defined
        // app.use(cors({
        //     origin: true,
        //     credentials: true
        // }));

        // To Serve static files for the frontend
        app.use(express.static(process.cwd() + "/dist/client"));

        // production
        if (process.env.NODE_ENV === "production") {

            // Rate limiting
            app.use(rateLimit({
                windowMs: 1000 * 60, // 1 minute
                max: 100, // 100 requests
                message: "Too Many Requests!",
                headers: true
            }));

            // Compression with zlib
            app.use(compression());
        }

        // Application Middleware to parse requests with Content-Type application/json
        app.use(express.json());

        // Disable the "X-Powered-By" Header on responses, avoiding giving hackers/crackers clues of
        // what tech is being used
        app.set("x-powered-by", false);
    });

    // Definição de configuração para ser aplicado à aplicação em caso de Exceções/Erros
    server.setErrorConfig((app: express.Application) => {

        //--- Middlewares a nível de aplicação para tratamento de Exceções/Erros

        // Error Handler padrão
        const errorHandler: IErrorHandlerAppMiddleware = container.get(IErrorHandlerAppMiddleware);
        app.use(errorHandler.handler.bind(errorHandler));
    });

    console.log("\tOK!");

    return server;
}

/**
 * Starts the application server.
 */
async function startServer(inversifyExpressServer: InversifyExpressServer): Promise<http.Server> {

    console.log("Building and Starting Application Server...");

    const port: number = Number(process.env.PORT);
    const application: express.Application = inversifyExpressServer.build();

    // Send te angular application when hit index
    // We could use an Regular Expression and set this configuration inside configureServer method,
    // but ordering the handlers correctly might perform better
    application.get("/*", (req: express.Request, res: express.Response) => {

        res.sendFile(process.cwd() + "/dist/client/index.html");
    });

    // Here we can configure the application to use SSL if on production
    // using plain HTTP instead

    const httpServer = http.createServer(application);
    const listenPromise = new Promise((resolve, reject) => {

        const errorListener: (error: Error) => void = error => reject(error);

        httpServer.addListener("error", errorListener);
        httpServer.listen(port, () => {

            httpServer.removeListener("error", errorListener);
            resolve();
        });
    });

    await listenPromise;
    console.log(`\tOK! Server is listening on port: ${port} and on HTTP mode`);
    return httpServer;
}

/**
 * Configures "SIGINT" to allow "Graceful Shutdown".
 */
async function configureShutdown(httpServer: http.Server): Promise<void> {

    console.log("Configuring Graceful Shutdown...");

    process.on("SIGINT", () => {

        httpServer.close((error: Error) => {

            if (error != null) {
                console.error(error);
                process.exit(1);
            }
            else {
                process.exit(0);
            }
        });
    });

    console.log("\tOK!");
}
