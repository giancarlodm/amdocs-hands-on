/**
 * This file includes polyfills used by the Server and must be loaded before anything else.
 *
 * You might include other polyfills if necessary.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */

import "reflect-metadata"; // Support for metadata, augment JavaScript Reflect Object
