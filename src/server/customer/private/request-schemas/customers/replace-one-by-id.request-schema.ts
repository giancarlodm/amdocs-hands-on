import { ConvenienceMethods } from "../../../../core/common";
import { RequestSchema } from "../../../../core/schema-validation";
import { CustomerSituationEnum } from "../../../shared/enums";

/**
 * JSON Schemas for {@link CustomersController.replaceOneById}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export const replaceOneByIdRequestSchema: RequestSchema = {

    params: {
        type: "object",
        required: ["id"],
        additionalProperties: false,
        properties: {
            id: {
                type: ["null", "integer"],
                minimum: 0
            }
        }
    },

    body: {
        type: "object",
        required: ["name"],
        additionalProperties: false,
        properties: {
            // Id is accepted, but the one informed on the URL prevails
            id: {
                type: ["null", "integer"]
            },
            name: {
                type: "string",
                maxLength: 500
            },
            situation: {
                type: "string",
                enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
            }
        }
    }
};
