import { ConvenienceMethods } from "../../../../core/common";
import { RequestSchema } from "../../../../core/schema-validation";
import { CustomerSituationEnum } from "../../../shared/enums";

/**
 * JSON Schemas for {@link CustomersController.insertOne}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export const insertOneRequestSchema: RequestSchema = {

    body: {
        type: "object",
        required: ["name", "situation"],
        additionalProperties: false,
        properties: {
            name: {
                type: "string",
                maxLength: 500
            },
            situation: {
                type: "string",
                enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
            }
        }
    }
};
