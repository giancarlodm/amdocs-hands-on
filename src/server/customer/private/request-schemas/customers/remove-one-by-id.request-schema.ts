import { RequestSchema } from "../../../../core/schema-validation";

/**
 * JSON Schemas for {@link CustomersController.removeOneById}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export const removeOneByIdRequestSchema: RequestSchema = {

    params: {
        type: "object",
        required: ["id"],
        additionalProperties: false,
        properties: {
            id: {
                type: ["null", "integer"],
                minimum: 1
            }
        }
    }
};
