import { ConvenienceMethods } from "../../../../core/common";
import { RequestSchema } from "../../../../core/schema-validation";
import { CustomerSituationEnum } from "../../../shared/enums";

/**
 * JSON Schemas for {@link CustomersController.find}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export const findRequestSchema: RequestSchema = {

    query: {
        type: "object",
        additionalProperties: false,
        properties: {
            filters: {
                type: "object",
                additionalProperties: false,
                properties: {
                    firstName: {
                        type: "string",
                        maxLength: 250
                    },
                    middleName: {
                        type: "string",
                        maxLength: 250
                    },
                    lastName: {
                        type: "string",
                        maxLength: 250
                    },
                    situation: {
                        type: "string",
                        enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
                    }
                }
            },
            options: {
                limit: {
                    type: "integer",
                    minimum: 0
                },
                skip: {
                    type: "integer",
                    minimum: 0
                }
            }
        }
    }
};
