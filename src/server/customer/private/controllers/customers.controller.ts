import {
    BaseHttpController, Controller, HttpGet, HttpPost, QueryParam, Response, RequestBody, HttpPut,
    RequestParam, HttpPatch, HttpDelete
} from "@weefsell/inversify-express";
import { inject as Inject } from "inversify";

import * as express from "express";
import { HttpSuccessStatusEnum } from "../../../core/http/status";

import { IRequestValidatorMiddleware } from "../../../core/schema-validation";
import { Customer } from "../../shared/entities";
import { ICustomersService } from "../../shared/services";
import * as requestSchemas from "../request-schemas/customers";
import * as queryParams from "../../shared/common/query-params/customers";

/**
 * @Controller
 * REST controller for customer resource.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@Controller("/customers")
class CustomersController extends BaseHttpController {

    //#region Private Attributes
    /**
     * See {@link ICustomersService}
     */
    private readonly customersService: ICustomersService;
    //#endregion

    //#region Constructor
    constructor(@Inject(ICustomersService) customersService: ICustomersService) {
        super();

        this.customersService = customersService;
    }
    //#endregion

    //#region HTTP CRUD Methods
    /**
     * Retrieves the list of customers.
     * TODO if pagination is required, an object with pagination metadata (total, limit, skip) must
     *  be returned instead of a plain Array
     * @param res Response object.
     * @param queryParams Query parameters.
     */
    @HttpGet(
        "/",
        {identifier: IRequestValidatorMiddleware, context: requestSchemas.findRequestSchema}
    )
    public async find(@Response() res: express.Response,
                      @QueryParam() queryParams: queryParams.Find): Promise<void> {

        const customers: Array<Customer> = await this.customersService.find(queryParams);
        res.status(HttpSuccessStatusEnum.OK).json(customers);
    }

    /**
     * Inserts a customer. Returns the generated id.
     * @param res Response object.
     * @param body Request body with the customer to insert.
     */
    @HttpPost(
        "/",
        {identifier: IRequestValidatorMiddleware, context: requestSchemas.insertOneRequestSchema}
    )
    public async insertOne(@Response() res: express.Response,
                           @RequestBody() body: Partial<Customer>): Promise<void> {

        const customer: Customer = new Customer();
        customer.id = undefined;
        customer.name = body.name;
        customer.situation = body.situation;

        await this.customersService.save(customer);

        res.status(HttpSuccessStatusEnum.CREATED).json({id: customer.id});
    }

    /**
     * Replaces an customer.
     * @param res Response object.
     * @param id The id of the customer to replace.
     * @param body The request body with the customer replacement.
     */
    @HttpPut(
        "/:id",
        {identifier: IRequestValidatorMiddleware, context: requestSchemas.replaceOneByIdRequestSchema}
    )
    public async replaceOneById(@Response() res: express.Response,
                                @RequestParam("id") id: number,
                                @RequestBody() body: Partial<Customer>): Promise<void> {

        const customer: Customer = new Customer();
        customer.id = id;
        customer.name = body.name;
        customer.situation = body.situation;

        await this.customersService.save(customer);

        res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
    }

    /**
     * Performs a partial update on the customer.
     * @param res Response object.
     * @param id The id of the customer to partialy update.
     * @param body The request body with the update to perform.
     */
    @HttpPatch(
        "/:id",
        {identifier: IRequestValidatorMiddleware, context: requestSchemas.patchOneByIdRequestSchema}
    )
    public async patchOneById(@Response() res: express.Response,
                              @RequestParam("id") id: number,
                              @RequestBody() body: Partial<Customer>): Promise<void> {

        await this.customersService.patchOneById(id, body);

        res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
    }

    /**
     * Removes a customer.
     * @param res Response Object
     * @param id The id of the customer to delete.
     */
    @HttpDelete(
        "/:id",
        {identifier: IRequestValidatorMiddleware, context: requestSchemas.removeOneByIdRequestSchema}
    )
    public async removeOneById(@Response() res: express.Response,
                               @RequestParam("id") id: number): Promise<void> {

        await this.customersService.removeOneById(id);
        res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
    }
}
