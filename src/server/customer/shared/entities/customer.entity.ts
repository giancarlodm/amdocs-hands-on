import { CustomerSituationEnum } from "../enums";

/**
 * A customer.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export class Customer {

    //#region Public Attributes
    /**
     * Customer's id.
     */
    public id: number;
    /**
     * Customer's name.
     */
    public name: string;
    /**
     * The customer situation.
     */
    public situation: CustomerSituationEnum;
    //#endregion
}
