import { CustomerSituationEnum } from "../../../enums";

/**
 * Query Params for {@link ICustomersService.find} method.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export type Find = {

    filters?: {
        situation?: CustomerSituationEnum
    },
    options?: {
        limit: number,
        skip: number
    }
}
