/**
 * Enum representing the situation of a {@link Customer}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @sice 02/03/2020
 */
export enum CustomerSituationEnum {

    REGISTERED = "REGISTERED",
    PENDING = "PENDING",
    CANCELLED = "CANCELLED"
}
