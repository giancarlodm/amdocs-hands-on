import { Find } from "../common/query-params/customers";
import { Customer } from "../entities";

/**
 * Business logic service for {@link Customer}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
export interface ICustomersService {

    /**
     * Retrieves the list of customers.
     * @param queryParams Parameters for conditions and pagination.
     * @return List of customers
     */
    find(queryParams?: Find): Promise<Array<Customer>>;
    /**
     * Inserts or updates a customer. During insertion, the id is generated and set on the entity.
     * @param customer the customer to be saved.
     * @return The customer saved. Same instance as the received parameter.
     */
    save(customer: Customer): Promise<Customer>;
    /**
     * Partially updates an customer.
     * @param id The id of the customer to patch.
     * @param partialCustomer The update to perform.
     */
    patchOneById(id: number, partialCustomer: Partial<Customer>): Promise<void>;
    /**
     * Removes a customer
     * @param id The id of the customer to remove
     */
    removeOneById(id: number): Promise<void>;
}

export const ICustomersService: symbol = Symbol("ICustomersService");
