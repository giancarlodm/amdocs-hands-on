import { inject as Inject } from "inversify";

import { IEntityManager } from "../../../../core/database";
import { ProvideRequest } from "../../../../core/dependency-injection";
import { HttpNotFoundException } from "../../../../core/http/exceptions/client-errors";
import { Find } from "../../common/query-params/customers";
import { Customer } from "../../entities";
import { ICustomersService } from "../customers.service.interface";

/**
 * @ProvideRequest
 * See {@link ICustomersService}
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@ProvideRequest(ICustomersService)
class CustomersService implements ICustomersService {

    //#region Private Attributes
    /**
     * The Entity Manager.
     * @injected
     */
    private readonly entityManager: IEntityManager;
    //#endregion

    //#region Constructor
    constructor(@Inject(IEntityManager) entityManager: IEntityManager) {

        this.entityManager = entityManager;
    }
    //#endregion

    //#region ICustomersService Methods
    /**
     * @inheritDoc
     */
    public async find(queryParams?: Find): Promise<Array<Customer>> {

        if (queryParams != null) {
            return await this.entityManager.find<Customer>(Customer, queryParams.filters, queryParams.options);
        }
        else {
            return this.entityManager.find(Customer);
        }
    }

    /**
     * @inheritDoc
     */
    public async patchOneById(id: number, partialCustomer: Partial<Customer>): Promise<void> {

        const modified: {modified: number} = await this.entityManager.update(Customer, {id: id}, partialCustomer);

        if (modified.modified === 0) {
            throw new HttpNotFoundException("Customer not found", {
                entity: Customer.name,
                id: id
            });
        }
    }

    /**
     * @inheritDoc
     */
    public async removeOneById(id: number): Promise<void> {

        const removed: {removed: number} = await this.entityManager.delete(Customer, {id: id});
        if (removed.removed === 0) {
            throw new HttpNotFoundException("Customer not found", {
                entity: Customer.name,
                id: id
            });
        }
    }

    /**
     * @inheritDoc
     */
    public async save(customer: Customer): Promise<Customer> {

        // An replace, check if customer exists
        if (customer.id != null) {

            const count: number = await this.entityManager.count(Customer, {id: customer.id}, {limit: 1, skip: 0});
            if (count === 0) {
                throw new HttpNotFoundException("Customer not found", {
                    entity: Customer.name,
                    id: customer.id
                });
            }
        }

        return this.entityManager.save(customer);
    }
    //#endregion
}
