import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

/**
 * @NgModule
 * Root routing module.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@NgModule({
    imports: [
        RouterModule.forRoot([
            // Index
            {
                path: "",
                pathMatch: "full",
                redirectTo: "customers"
            },
            // --- Application Modules (Lazy Loaded)
            // Customers
            {
                path: "customers",
                loadChildren: () => import("./customers/private/customers.module").then(m => m.CustomersModule)
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
