import { Component, ViewEncapsulation } from "@angular/core";

@Component({
    selector: "amdocs",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    title = "amdocs-hands-on-client";
}
