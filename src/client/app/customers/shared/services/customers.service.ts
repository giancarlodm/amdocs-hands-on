import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import formUrlEncoded from "form-urlencoded";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Find } from "../../../../../server/customer/shared/common/query-params/customers";

// We should "duplicate" the entity code from the server and the client (ideal), because if we use TypeORM
// to define persistent entity classes, those decorators does not apply on client code.
import { Customer } from  "../../../../../server/customer/shared/entities";
import { environment } from "../../../../environments/environment";

/**
 * Business logic proxy service for {@link Customer}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@Injectable({
    providedIn: "root"
})
export class CustomersService {

    //#region Private Attributes
    /**
     * Angular HTTP service to perform Http Requests.
     */
    private readonly httpClient: HttpClient;
    /**
     * The resource URL bound to this proxy service.
     */
    private readonly resourceUrl: string;
    //#endregion

    //#region Constructor
    constructor(httpClient: HttpClient) {

        this.httpClient = httpClient;
        this.resourceUrl = `${environment.urls.api}/customers`;
    }
    //#endregion

    //#region Public Methods
    /**
     * Retrieves the list of customers.
     * @param queryParams Parameters for conditions and pagination.
     * @return List of customers
     */
    public find(queryParams?: Find): Observable<Array<Customer>> {

        const queryParamsEncoded: string = formUrlEncoded(queryParams);
        const urlQueryParams: string = queryParamsEncoded != null && queryParamsEncoded.length > 0 ? `?${queryParamsEncoded}` : "";
        const url: string = `${this.resourceUrl}${urlQueryParams}`;

        return this.httpClient
            .get<Array<Partial<Customer>>>(url)
            .pipe(
                map(customersJson => {

                    // TODO this process can be automated with my package @enterprize/serializer, to
                    //  fully restore the prototype.
                    const customers: Array<Customer> = [];
                    for (let customerJson of customersJson) {

                        const customer: Customer = new Customer();
                        customer.id = customerJson.id;
                        customer.name = customerJson.name;
                        customer.situation = customerJson.situation;

                        customers.push(customer);
                    }

                    return customers;
                })
            );
    }

    /**
     * Inserts or updates a customer. During insertion, the id is generated and set on the entity.
     * @param customer the customer to be saved.
     * @return The customer saved. Same instance as the received parameter.
     */
    public save(customer: Customer): Observable<Customer> {

        // Edition
        if (customer.id != null) {

            const url: string = `${this.resourceUrl}/${customer.id}`;
            return this.httpClient
                .put<void>(
                    url,
                    customer
                )
                .pipe(
                    map(() => {
                        return customer;
                    })
                );
        }
        // Insertion
        else {

            const url: string = `${this.resourceUrl}`;
            return this.httpClient
                .post<Pick<Customer, "id">>(
                    url,
                    customer
                )
                .pipe(
                    map(value => {
                        customer.id = value.id;
                        return customer;
                    })
                );
        }
    }

    /**
     * Partially updates an customer.
     * @param id The id of the customer to patch.
     * @param partialCustomer The update to perform.
     */
    public patchOneById(id: number, partialCustomer: Partial<Customer>): Observable<void> {

        const url: string = `${this.resourceUrl}/${id}`;
        return this.httpClient.patch<void>(
            url,
            partialCustomer
        );
    }

    /**
     * Removes a customer
     * @param id The id of the customer to remove.
     */
    public removeOneById(id: number): Observable<void> {

        const url: string = `${this.resourceUrl}/${id}`;

        return this.httpClient
            .delete<void>(
                url
            );
    }
    //#endregion
}
