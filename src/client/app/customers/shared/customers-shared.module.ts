import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

/**
 * @NgModule
 * Shared module of {@link CustomersModule}. Provides service, pipes and components that are public
 * to other modules.
 */
@NgModule({
    imports: [
        // Angular Dependencies
        CommonModule
    ]
})
export class CustomersSharedModule {

}
