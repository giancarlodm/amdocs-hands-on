import {
    Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild
} from "@angular/core";
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";

import { Customer } from "../../../../../../server/customer/shared/entities";
import { CustomerSituationEnum } from "../../../../../../server/customer/shared/enums";
import { CustomersService } from "../../../shared/services";

/**
 * @Component
 * Saves and updates customers.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@Component({
    selector: "customer-save",
    templateUrl: "./customer-save.component.html",
    styleUrls: ["./customer-save.component.scss"]
})
export class CustomerSaveComponent implements OnChanges {

    //#region Inputs
    /**
     * Customer to update
     */
    @Input()
    public customer: Customer;
    //#endregion

    //#region Outputs
    /**
     * Event emiter when a customer is saved.
     */
    @Output()
    public onSave: EventEmitter<Customer> = new EventEmitter<Customer>();
    //#endregion

    //#region Public Attributes
    /**
     * See {@link CustomerSituationEnum}
     */
    public readonly CustomerSituationEnum: typeof CustomerSituationEnum = CustomerSituationEnum;
    /**
     * The form.
     */
    public formGroup: FormGroup;
    /**
     * NgForm directive. Used to reset the submitted status.
     */
    @ViewChild("ngForm", {static: true})
    public ngForm: NgForm;
    /**
     * Flag that indicate a save operation is in progress.
     */
    public isSaving: boolean;
    //#endregion

    //#region Private Attributes
    /**
     * See {@link CustomersService}.
     */
    private readonly customersService: CustomersService;
    //#endregion

    //#region Constructor
    constructor(customersService: CustomersService) {

        this.customersService = customersService;

        this.isSaving = false;
        this.formGroup = new FormGroup({
            name: new FormControl(null, [Validators.required, Validators.maxLength(500)]),
            situation: new FormControl(null, [Validators.required])
        });
    }
    //#endregion

    //#region Lifecycle Hooks
    /**
     * @inheritDoc
     */
    public ngOnChanges(changes: SimpleChanges): void {

        if (changes.customer != null) {

            if (this.customer != null) {

                this.ngForm.resetForm();
                this.formGroup.reset({
                    name: this.customer.name,
                    situation: this.customer.situation
                });
            }
            else {
                this.ngForm.resetForm();
                this.formGroup.reset();
            }
        }
    }
    //#endregion

    //region Public Methods
    /**
     * Saves the current customer's form data.
     */
    public save(): void {

        this.isSaving = true;

        const formValue: Partial<Customer> = this.formGroup.value;

        const customer: Customer = this.customer != null ? this.customer : new Customer();
        customer.name = formValue.name;
        customer.situation = formValue.situation;

        this.customersService.save(customer)
            .subscribe(
                (customer: Customer) => {

                    this.isSaving = false;
                    this.ngForm.resetForm();
                    this.formGroup.reset();
                    this.onSave.emit(customer);
                },
                error => {

                    //TODO should have a better error treatment, like a service with a dialog or a toast
                    console.error(error);
                    alert(JSON.stringify(error));
                }
            );
    }
    //#endregion
}
