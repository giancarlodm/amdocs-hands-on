import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Customer } from "../../../../../../server/customer/shared/entities";
import { CustomerSituationEnum } from "../../../../../../server/customer/shared/enums";
import { CustomersService } from "../../../shared/services";

/**
 * @Component
 * Display and updates the customer situation.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@Component({
    selector: "customer-situation",
    templateUrl: "./customer-situation.component.html",
    styleUrls: ["./customer-situation.component.scss"]
})
export class CustomerSituationComponent {

    //#region Inputs
    /**
     * The customer to display/modify the situation
     */
    @Input()
    public customer: Customer;
    //#endregion

    //#region Outputs
    /**
     * Event emitter whe customer situation changes.
     */
    @Output()
    public situationChange: EventEmitter<CustomerSituationEnum> = new EventEmitter<CustomerSituationEnum>();
    //#endregion

    //#region Public Attributes
    /**
     * See {@link CustomerSituationEnum}.
     */
    public readonly CustomerSituationEnum: typeof CustomerSituationEnum = CustomerSituationEnum;
    /**
     * Flag that indicates an update is in progress.
     * TODO with this flag, an animation icon can be show on the UI
     */
    public isUpdating: boolean;
    //#endregion

    //#region Private Attributes
    /**
     * See {@link CustomersService}.
     */
    private readonly customersService: CustomersService;
    //#endregion

    //#region Constructor
    constructor(customersService: CustomersService) {

        this.customersService = customersService;
        this.isUpdating = false;
    }
    //#endregion

    //#region Public Methods
    /**
     * Updates the customer situation.
     * @param situation
     */
    public updateSituation(situation: CustomerSituationEnum): void {

        this.isUpdating = true;

        this.customersService.patchOneById(this.customer.id, {situation: situation})
            .subscribe(
                () => {

                    this.isUpdating = false;
                    this.customer.situation = situation;
                    this.situationChange.emit(situation);
                },
                error => {

                    //TODO should have a better error treatment, like a service with a dialog or a toast
                    console.error(error);
                    alert(JSON.stringify(error));
                }
            );
    }
    //#endregion
}
