import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { distinctUntilChanged } from "rxjs/operators";
import { Find } from "../../../../../../server/customer/shared/common/query-params/customers";

import { Customer } from "../../../../../../server/customer/shared/entities";
import { CustomerSituationEnum } from "../../../../../../server/customer/shared/enums";
import { CustomersService } from "../../../shared/services";

/**
 * @Component
 * View that display registered customers and also allow for inserting customer.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@Component({
    selector: "customers-list-view",
    templateUrl: "./customers-list.view.html",
    styleUrls: ["./customers-list.view.scss"]
})
export class CustomersListView implements OnInit {

    //#region Public Attributes
    /**
     * List of registered customers.
     */
    public customers: Array<Customer>;
    /**
     * Flag tha indicates a fetching is in progress.
     */
    public isLoading: boolean;
    /**
     * The selected filter.
     */
    public selectedFilter: FormControl;
    /**
     * See {@link CustomerSituationEnum}
     */
    public readonly CustomerSituationEnum: typeof CustomerSituationEnum = CustomerSituationEnum;
    //#endregion

    //#region Private Attributes
    /**
     * See {@link CustomersService}.
     */
    private readonly customersService: CustomersService;
    //#endregion

    //#region Constructor
    constructor(customersService: CustomersService) {

        this.customersService = customersService;

        this.selectedFilter = new FormControl("ALL");
        this.isLoading = false;
    }
    //#endregion

    //#region Lifecycle Methods
    /**
     * @inheritDoc
     */
    public ngOnInit(): void {

        this.fetchData();
        this.selectedFilter.valueChanges
            .pipe(distinctUntilChanged())
            .subscribe(() => this.fetchData());
    }
    //#endregion

    //#region Public Methods
    /**
     * Remove a customer.
     * @param customer The customer to remove.
     */
    public remove(customer: Customer): void {

        this.customersService.removeOneById(customer.id)
            .subscribe(
                () => {
                    this.customers.splice(this.customers.indexOf(customer), 1);
                },
                error =>{

                    // this.isLoading = false;
                    console.error(error);
                    alert(JSON.stringify(error));
                }
            );
    }

    /**
     * Listener of adding customer. Adds or updates the current list of customers without re-fetching
     * data.
     * @param customer The customer added or updated.
     */
    public onSave(customer: Customer): void {

        const index: number = this.customers.findIndex(value => value.id === customer.id);
        if (index != -1) {
            this.customers[index] = customer;
        }
        else {
            this.customers.push(customer);
        }
    }
    //#endregion

    //#region Private Methods
    /**
     * Fetches data from the server using the business proxy service.
     */
    private fetchData(): void {

        this.isLoading = true;

        const queryParams: Find = {};
        if (this.selectedFilter.value !== "ALL") {

            queryParams.filters = {};
            queryParams.filters.situation = this.selectedFilter.value;
        }

        this.customersService.find(queryParams)
            .subscribe(
                (customers: Array<Customer>) => {

                    this.customers = customers;
                    this.isLoading = false;
                },
                error =>{

                    this.isLoading = false;
                    console.error(error);
                    alert(JSON.stringify(error));
                });
    }
    //#endregion
}
