import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { CustomersListView } from "./views";

/**
 * @NgModule
 * Routing module for {@link CustomersModule}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                data: {
                    title: "Customers"
                },
                children: [
                    {
                        path: "",
                        component: CustomersListView
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class CustomersRoutingModule {

}
