import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CustomersSharedModule } from "../shared/customers-shared.module";

import { CustomerSaveComponent, CustomerSituationComponent } from "./components";
import { CustomersRoutingModule } from "./customers-routing.module";
import { CustomersListView } from "./views";

/**
 * @NgModule
 * Customers module. Displays and manipulates customer data.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
@NgModule({
    imports: [
        // Angular Dependencies
        CommonModule,
        ReactiveFormsModule,
        // Routing
        CustomersRoutingModule,
        // SharedModule
        CustomersSharedModule
    ],
    declarations: [
        // Components
        CustomerSaveComponent,
        CustomerSituationComponent,
        // Views
        CustomersListView,
    ]
})
export class CustomersModule {

}
