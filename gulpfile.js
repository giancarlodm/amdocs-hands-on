"use strict";

const gulp = require("gulp");
const gulpTypescript = require("gulp-typescript");
const del = require("del");
const exec = require('child_process').exec;

const typescriptProject = gulpTypescript.createProject("src/server/tsconfig.json", {
	removeComments: true
});
const typescriptDeclarationProject = gulpTypescript.createProject("src/server/tsconfig.json", {
	declaration: true,
	noResolve: false,
	emitDeclarationOnly: true,
	removeComments: false
});

function clean() {

	return del(["dist/server"]);
}

function build() {

	return gulp.src(["src/server/**/*.ts", "!src/server/**/*.spec.ts"])
		.pipe(typescriptProject())
		.on("error", function(err) {
			console.error(err);
			process.exit(1);
		})
		.pipe(gulp.dest("dist/server"));
}

function buildDeclaration() {

	return gulp.src(["src/server/**/*.ts", "!src/server/**/*.spec.ts"])
		.pipe(typescriptDeclarationProject())
		.on("error", function(err) {
			console.error(err);
			process.exit(1);
		})
		.pipe(gulp.dest("dist/server"));
}

function buildClient(cb) {

	exec("ng build --prod", (err, stdout, stderr) => {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
}

const buildTask = gulp.series(clean, build, buildDeclaration, buildClient);

exports.default = buildTask;
