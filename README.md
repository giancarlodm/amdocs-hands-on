# Amdocs Hands-On Exercise TypeScript + Inversify version

## Running

- You need NodeJs 12.x installed, you can obtain here [NodeJS](https://nodejs.org/en/).

### On Production

#### 1 - Build the application: 

- Run ``npm install``
- Run ``gulp``
- Congratulations, the build is complete :)

#### 2 - Execute

- Linux:
  - Run ``npm run start``
- Windows:
  - Run ``npm run start_windows``

#### If you are going to deploy

- Run the ``npm install``
- Build with ``gulp``
- Copy ``dist``, ``package.json``, ``package-lock.json`` to the deploy location
- Run ``npm install --production``. This prevents development packages being installed without necessity and reducing node_modules size. 
- Run ``npm run start`` or ``npm run start_windows`` or use [PM2](https://pm2.io/docs/plus/overview/) to manage NodeJS process.

### On Development

- Run ``npm install``
- Open a terminal/cmd/PowerShell and run ``ng build --watch``
- Open another terminal/cmd/PowerShell and run ``npm run dev`` or ``npm run dev_windows``


## Development notes

I choose to use [TypeScript](https://github.com/microsoft/TypeScript) with [Inversify](https://github.com/inversify) and [inversify-express](https://github.com/weefsell/inversify-express) (a package of my own authoring forked from [inversify-express-utils](https://github.com/inversify/inversify-express-utils)) and [Angular](https://github.com/angular/angular) because I prefer a more OOP coding style and I'm more familiar with. 

But there is some trade-offs with this approach:
- On the server side, using Inversify with inversify-express there is a performance penalty due to the instantiation of controllers, services and child containers on every request. The V8 JavaScript VM manages very well object creations and actually reuses them if needed (like the JVM), but there is still some penalty.

There is other advanced features that I used on my startup days such as ORM framework (TypeORM) and decorators to control transaction flows across multiple services, but it is too complex and overkill for this exercise.

I have no problem working with TypeScript and pure ExpressJS without those fancy DI containers and decorators. I can even work with pure JavaScript, although not recommended for big and complex projects (Strong Typing reduces a lot of bugs). I just prefer OOP and declarative coding style due to my background in Java (desktop and JSF) on my early programming days. Check the project amdocs-hands-on-pure-js to see a version of this server in pure JavaScript and ExpressJS and I tried using React. 

About React. I never worked with it, but if knowing it is a must, I can learn it (it has a much easier leaning curve than Angular).

Also, I have Java background. I used to develop desktop apps with Swing and JavaFX and Web apps with JSF, REST and Hibernate during my technical school and college days. So, learning Spring Boot will be much easier. 
